<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Rodacker.'.$_EXTKEY,
    'pi1',
    'LLL:EXT:google_maps_pages/Resources/Private/Language/locallang_be.xlf:pi1_title'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY,
    'Configuration/TypoScript',
    'Google Maps Pages'
);
?>