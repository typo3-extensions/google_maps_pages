#
# Table structure for table 'pages'
#
CREATE TABLE pages (
	tx_googlemapspages_address text,
	tx_googlemapspages_longitude tinytext,
	tx_googlemapspages_latitude tinytext
);

#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
  tx_googlemapspages_address text,
  tx_googlemapspages_longitude tinytext,
  tx_googlemapspages_latitude tinytext
);