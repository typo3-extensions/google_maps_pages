<?php

/**
 * Enter description here ...
 * @author rodi
 *
 */
class tx_googlemapspages_tcemain_processdatamap {
 
	function processDatamap_postProcessFieldArray($status, $table, $id, &$fieldArray, &$reference) {
		
		if ($table == 'pages' || $table == 'tt_content') {

			// get record
			$oldRecord = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('*', $table, 'uid = ' . $id);
			
			// merge with submitted data
			$record = array_merge($oldRecord, $fieldArray);
			
			// address geocoding			
			if ($address = $record['tx_googlemapspages_address']) {
				
				$coordinates = $this->geocodeAddress($address);
				
				$fieldArray['tx_googlemapspages_longitude'] = $coordinates['lng'];
				$fieldArray['tx_googlemapspages_latitude'] 	= $coordinates['lat'];
			} 
			
			// tt_content geocoding in plugin flexform 
			if ($record['list_type'] && $record['list_type'] == 'google_maps_pages_pi1' && $record['pi_flexform']) {
				$flexform = t3lib_div::xml2array($record['pi_flexform']);
				if ($flexform['data']['sDEFAULT']['lDEF']['address']['vDEF']) {	
					$coordinates = $this->geocodeAddress($flexform['data']['sDEFAULT']['lDEF']['address']['vDEF']);
					$flexform['data']['sDEFAULT']['lDEF']['longitude']['vDEF'] = $coordinates['lng'];
					$flexform['data']['sDEFAULT']['lDEF']['latitude']['vDEF'] = $coordinates['lat'];
					$fieldArray['pi_flexform'] = t3lib_div::array2xml($flexform);
				}
			}	
		}
	}
	
	/**
	 * Takes an address as parameter an uses the google geocoder api v3 to retrieve
	 * the longitude and latitude
	 * 
	 * @param String $address
	 */
	function geocodeAddress($address) {
		
		$confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['google_maps_pages']);
		
		$result = false;
		$status = false;
		$url = 'http://maps.google.com/maps/api/geocode/json';
		$url .= '?address=' . rawurlencode($address);
		$url .= '&sensor=false';
		$url .= '&key=' . $confArr['key'];
		
		if ($confArr['debug']) {
			t3lib_div::debug(array(
				'address' 	=> $address,
				'url' 		=> $url
			));
		}		
		
		$coordinates = array();
				
		if ($json = file_get_contents($url)) {			
	
			
		} else {
			$json = $this->file_get_contents_curl($url);
		}
		
		$result = json_decode($json, true);
			
		if ($confArr['debug']) {
			t3lib_div::debug(array(
				'json' 		=> $json,
				'result' 	=> $result,
			));
		}
		
		if ($result['status'] == 'OK') {		
			$coordinates['lng'] = $result['results'][0]['geometry']['location']['lng'];
			$coordinates['lat'] = $result['results'][0]['geometry']['location']['lat'];	
			
			if ($confArr['debug']) {
				t3lib_div::debug(array(  'coordinates' => $coordinates ));
			}
				
			return $coordinates;
		}
		
		return $result;
	}
	 
	function file_get_contents_curl($url) {
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
		curl_setopt($ch, CURLOPT_URL, $url);
		
		$data = curl_exec($ch);
		curl_close($ch);
		
		return $data;
	}
}