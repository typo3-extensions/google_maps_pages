<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 09.01.16
 * Time: 18:44
 */

$tempColumns = [
    'tx_googlemapspages_address' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:google_maps_pages/Resources/Private/Language/locallang_db.xlf:pages.tx_googlemapspages_address',
        'config' => [
            'type' => 'text',
            'cols' => '30',
            'rows' => '5',
        ]
    ],
    'tx_googlemapspages_latitude' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:google_maps_pages/Resources/Private/Language/locallang_db.xlf:pages.tx_googlemapspages_latitude',
        'config' => [
            'type' => 'input',
            'size' => '30',
        ]
    ],
    'tx_googlemapspages_longitude' => [
        'exclude' => 0,
        'label' => 'LLL:EXT:google_maps_pages/Resources/Private/Language/locallang_db.xlf:pages.tx_googlemapspages_longitude',
        'config' => [
            'type' => 'input',
            'size' => '30',
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    '--div--;LLL:EXT:google_maps_pages/Resources/Private/Language/locallang_db.xlf:tt_content.tab.geolocation,
    tx_googlemapspages_address;;;;1-1-1, tx_googlemapspages_latitude, tx_googlemapspages_longitude'
);



$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['googlemapspages_pi1'] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['googlemapspages_pi1'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'googlemapspages_pi1',
    'FILE:EXT:google_maps_pages/Configuration/FlexForms/flexform.xml'
);