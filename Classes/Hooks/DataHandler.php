<?php

namespace Rodacker\GoogleMapsPages\Hooks;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Hook into tcemain and geocode addresses in pages or tt_content tables
 *
 * @package TYPO3
 * @subpackage tx_googlemapspages
 */
class DataHandler
{

    /**
     * @param $status
     * @param $table
     * @param $id
     * @param $fieldArray
     * @param $reference
     */
    public function processDatamap_postProcessFieldArray($status, $table, $id, &$fieldArray, &$reference)
    {


        if ($table == 'pages' || $table == 'tt_content') {

            // get record
            $oldRecord = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('*', $table, 'uid = '.$id);

            // merge with submitted data
            $record = array_merge($oldRecord, $fieldArray);

            // address geocoding
            if ($address = $record['tx_googlemapspages_address']) {

                $coordinates = $this->geocodeAddress($address);

                $fieldArray['tx_googlemapspages_longitude'] = $coordinates['lng'];
                $fieldArray['tx_googlemapspages_latitude'] = $coordinates['lat'];
            }

            // tt_content geocoding in plugin flexform
            if ($record['list_type'] && $record['list_type'] == 'googlemapspages_pi1' && $record['pi_flexform']) {
                $flexform = GeneralUtility::xml2array($record['pi_flexform']);
                if ($flexform['data']['sDEFAULT']['lDEF']['address']['vDEF']) {
                    $coordinates = $this->geocodeAddress($flexform['data']['sDEFAULT']['lDEF']['address']['vDEF']);
                    $flexform['data']['sDEFAULT']['lDEF']['longitude']['vDEF'] = $coordinates['lng'];
                    $flexform['data']['sDEFAULT']['lDEF']['latitude']['vDEF'] = $coordinates['lat'];
                    $fieldArray['pi_flexform'] = GeneralUtility::array2xml($flexform);
                }
            }
        }
    }

    /**
     * Takes an address as parameter an uses the google geocoder api v3 to retrieve
     * the longitude and latitude
     *
     * @param String $address
     */
    private function geocodeAddress($address)
    {
        $confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['google_maps_pages']);

        $result = false;
        $status = false;
        $url = 'https://maps.google.com/maps/api/geocode/json';
        $url .= '?address='.rawurlencode($address);
        $url .= '&key='.$confArr['key'];

        if ($confArr['debug']) {
            DebuggerUtility::var_dump(
                [
                    'address' => $address,
                    'url' => $url
                ]
            );
        }

        $coordinates = [];

        if ($json = file_get_contents($url)) {
        } else {
            $json = $this->file_get_contents_curl($url);
        }

        $result = json_decode($json, true);

        if ($confArr['debug']) {
            DebuggerUtility::var_dump(
                [
                    'json' => $json,
                    'result' => $result,
                ]
            );
        }

        if ($result['status'] == 'OK') {
            $coordinates['lng'] = $result['results'][0]['geometry']['location']['lng'];
            $coordinates['lat'] = $result['results'][0]['geometry']['location']['lat'];

            if ($confArr['debug']) {
                DebuggerUtility::var_dump(['coordinates' => $coordinates]);
            }

            return $coordinates;
        }

        return $result;
    }

    /**
     * @param $url
     *
     * @return mixed
     */
    private function file_get_contents_curl($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt(
            $ch,
            CURLOPT_RETURNTRANSFER,
            1
        ); //Set curl to return the data instead of printing it to the browser.
        curl_setopt($ch, CURLOPT_URL, $url);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}