<?php
namespace Rodacker\GoogleMapsPages\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Patrick Rodacker <patrick.rodacker@gmail.com>, Patrick Rodacker - Software & Web Development
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Frontend\ContentObject\AbstractContentObject;

/**
 * MarkerController
 */
class MarkerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var TYPO3\CMS\Extbase\Configuration\ConfigurationManager
     * @inject
     */
    protected $configurationManager;

    /**
     * action javascript
     *
     * @return void
     */
    public function javascriptAction()
    {

        $contentObject = $this->configurationManager->getContentObject();

        // if pages ist not set, set to current page
        if (!$this->settings['pages']) {
            $this->settings['pages'] = $GLOBALS['TSFE']->id;
        }

        if (null == $this->settings['latitude'])
        {
            $this->settings['latitude'] = '53.1201552';
        }

        if (null == $this->settings['longitude'])
        {
            $this->settings['longitude'] = '8.5962021';
        }

        if (null == $this->settings['zoom'])
        {
            $this->settings['zoom'] = 5;
        }

        switch ($this->settings['type']) {

            case 'content':
                $markers = $this->getMarkersForContentFromPage();
                break;

            case 'single':
                $markers = $this->getMarkerForSinglePage();
                break;

            case 'subpages':
            default:
                $markers = $this->getMarkersForSubPages();
                break;
        }

        // set the unique id
        $contentId = $contentObject->data['uid'];


        $this->view->assignMultiple([
            'containerId' => 'map' . $contentId,
            'code' => $this->getJavascriptCode($markers, $contentId)
        ]);
    }

    /**
     * return the marker for the selected page or for the current one if none has beenselected
     *
     * @return Array
     */
    private function getMarkerForSinglePage()
    {

        // get single page
        $page = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('*', 'pages', 'uid='.intval($this->settings['pages']));

        $this->settings['latitude'] = $page['tx_googlemapspages_latitude'];
        $this->settings['longitude'] = $page['tx_googlemapspages_longitude'];

        $markers[] = $this->getMarkerFromPage($page);

        return $markers;
    }

    /**
     * return the markers for
     *
     * @return Array
     */
    private function getMarkersForSubPages()
    {

        $contentObject = $this->configurationManager->getContentObject();

        $markers = [];

        // get subpages of selected pages
        $result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            '*',
            'pages',
            'pid = '.intval(
                $this->settings['pages']
            ).' AND tx_googlemapspages_address IS NOT NULL'.$contentObject->enableFields('pages')
        );

        while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)) {
            $markers[] = $this->getMarkerFromPage($row);
        }

        return $markers;
    }

    /**
     * return the markers for
     *
     * @return Array
     */
    private function getMarkersForContentFromPage()
    {

        $contentObject = $this->configurationManager->getContentObject();

        $markers = [];

        // get subpages of selected pages
        $result = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            '*',
            'tt_content',
            'pid = '.intval(
                $this->settings['pages']
            ).' AND tx_googlemapspages_address IS NOT NULL'.$contentObject->enableFields('tt_content')
        );


        while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result)) {
            $markers[] = $this->getMarkerFromContent($row);
        }

        return $markers;
    }

    /**
     * builds and returns the single marker from the submitted page data
     *
     * @param Array $page
     *
     * @return Array
     */
    private function getMarkerFromPage($page)
    {

        $linkConf = [];
        $linkConf['parameter'] = $page['uid'];
        $contentObject = $this->configurationManager->getContentObject();
        $url = $contentObject->typoLink_URL($linkConf);

        // get language overlay
        if ($GLOBALS['TSFE']->sys_language_uid > 0) {
            $page = $GLOBALS['TSFE']->sys_page->getPageOverlay($page, $GLOBALS['TSFE']->sys_language_uid);
        }

        $marker = [
            'title' => $page['title'],
            'url' => $url,
            'latitude' => $page['tx_googlemapspages_latitude'],
            'longitude' => $page['tx_googlemapspages_longitude']
        ];

        return $marker;
    }

    /**
     * return a marker from a tt_content record
     *
     * @param unknown_type $record
     */
    private function getMarkerFromContent($record)
    {

        $marker = [
            'title' => $record['header'],
            'latitude' => $record['tx_googlemapspages_latitude'],
            'longitude' => $record['tx_googlemapspages_longitude']
        ];

        return $marker;
    }

    /**
     * return the javascript code to display the map and add markers
     *
     * @return String
     */
    private function getJavascriptCode($markers, $contentId)
    {

        // get key from extension configuration
        $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['google_maps_pages']);

        // add maps api to header
        $GLOBALS['TSFE']->additionalHeaderData['google_maps_pages'] = '<script type="text/javascript" src="http://maps.google.com/maps/api/js?key='.$extConf['key'].'"></script>';

        $mapId = 'map' . $contentId;
        $optionsId = 'options' . $contentId;
        $latlngId = 'latlng' .$contentId;

        $javascriptCode = '
			<script type="text/javascript">
    			var '.$latlngId.' = new google.maps.LatLng('.$this->settings['latitude'].','.$this->settings['longitude'].');
    			var '.$optionsId.' = {
      				zoom: '.$this->settings['zoom'].',
      				center: '.$latlngId.',
      				mapTypeId: google.maps.MapTypeId.TERRAIN
    			};
    			var '.$mapId.' = new google.maps.Map(document.getElementById("'.$mapId.'"), '.$optionsId.');';

        $counter = 0;
        foreach ($markers as $marker) {

            if ($marker['latitude'] || $marker['longitude']) {
                $markerId = 'marker'.$counter;
                $positionId = 'position'.$counter;
                $infowindowId = 'info'.$counter;

                $javascriptCode .= '
					var '.$markerId.' = new google.maps.Marker({
	      				position: new google.maps.LatLng('.$marker['latitude'].','.$marker['longitude'].'),
	      				map: '.$mapId.',
	      				title:"'.$marker['title'].'"
	  				});';

                if ($this->settings['type'] == 'subpages') {
                    $javascriptCode .= '
	  					google.maps.event.addListener('.$markerId.', "click", function() {
	  						window.location = "'.$marker['url'].'";
	  					});';
                }
            }
        }

        $javascriptCode .= '</script>';

        return $javascriptCode;
    }
}