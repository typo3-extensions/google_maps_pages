<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 09.01.16
 * Time: 20:39
 */

namespace Rodacker\GoogleMapsPages;


use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ext_update {

    const TABLE = 'tt_content';

    const PI1_KEY_OLD = 'google_maps_pages_pi1';
    const PI1_KEY_NEW = 'googlemapspages_pi1';

    const NEW_FLEXFORM_FIELDS_PREFIX = 'settings.';

    public function access()
    {
        return true;
    }

    public function main()
    {
        // update list_type entries
        $this->getDatabaseConnection()->exec_UPDATEquery(
            'tt_content',
            "list_type='".self::PI1_KEY_OLD."'",
            ['list_type' => self::PI1_KEY_NEW]
        );

        // update pi_flexform entries in xml format
        $query =
            "UPDATE " . self::TABLE . " SET
              `pi_flexform` = REPLACE(`pi_flexform`, 'type', 'settings.type'),
              `pi_flexform` = REPLACE(`pi_flexform`, 'pages', 'settings.pages'),
              `pi_flexform` = REPLACE(`pi_flexform`, 'address', 'settings.address'),
              `pi_flexform` = REPLACE(`pi_flexform`, 'longitude', 'settings.longitude'),
              `pi_flexform` = REPLACE(`pi_flexform`, 'latitude', 'settings.latitude'),
              `pi_flexform` = REPLACE(`pi_flexform`, 'width', 'settings.width'),
              `pi_flexform` = REPLACE(`pi_flexform`, 'height', 'settings.height'),
              `pi_flexform` = REPLACE(`pi_flexform`, 'zoom', 'settings.zoom')
              WHERE list_type = '" . self::PI1_KEY_NEW . "' AND
              pi_flexform LIKE '<?xml%';";

        $this->getDatabaseConnection()->sql_query($query);

        // update pi_flexform entries in php array format
        $query =
            "UPDATE " . self::TABLE ." SET
              `pi_flexform` = REPLACE(`pi_flexform`, '<type', '<settings.type'),
              `pi_flexform` = REPLACE(`pi_flexform`, '<pages', '<settings.pages'),
              `pi_flexform` = REPLACE(`pi_flexform`, '<address', '<settings.address'),
              `pi_flexform` = REPLACE(`pi_flexform`, '<longitude', '<settings.longitude'),
              `pi_flexform` = REPLACE(`pi_flexform`, '<latitude', '<settings.latitude'),
              `pi_flexform` = REPLACE(`pi_flexform`, '<width', '<settings.width'),
              `pi_flexform` = REPLACE(`pi_flexform`, '<height', '<settings.height'),
              `pi_flexform` = REPLACE(`pi_flexform`, '<zoom', '<settings.zoom')
              WHERE list_type = '". self::PI1_KEY_NEW ."' AND
              pi_flexform LIKE '<phparray%';";

        $this->getDatabaseConnection()->sql_query($query);

        /** @var \TYPO3\CMS\Core\Messaging\FlashMessage $flashMessage */
        $flashMessage = GeneralUtility::makeInstance(
            'TYPO3\\CMS\\Core\\Messaging\\FlashMessage',
            'Update of tt_content entries ok'
        );
        return $flashMessage->render();
    }

    /**
     * @return DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}