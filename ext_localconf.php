<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Rodacker.'.$_EXTKEY,
	'Pi1',
	[
		'Marker' => 'javascript',

	],
	// non-cacheable actions
	[
		'Marker' => 'javascript',

	]
);


// add hook implementation for tcemain class
$GLOBALS ['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] =
    \Rodacker\GoogleMapsPages\Hooks\DataHandler::class;