<?php
$EM_CONF[$_EXTKEY] = array(
	'title' => 'Google Maps Pages',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Patrick Rodacker',
	'author_email' => 'patrick.rodacker@gmail.com',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '7.6.1',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-7.6.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);